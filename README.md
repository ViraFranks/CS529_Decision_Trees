# CS529_Decision_Trees

Welcome to our code for building a decision tree and classifying DNA sequences.

Instructions for running the code in its current form are as follows,
1. Download or clone the folder to your local hard drive
2. Open a terminal and run dt.py with the python interpreter. Or open the dt.py
file with your favorite IDE (tested with IDLE on python 3.5 and 3.6) and run the
code.

Necessary File list:
dt.py (vital)
training.csv (vital)
testing.csv (vital)
README.md (obviously vital)

Other documents are of an informational nature only.

A sample example of how to run the code can be seen at the bottom of the file dt.py.
We will include a few examples below and describe how they work.

dt = Decision_Tree('training.csv', 'testing.csv')
dt = Decision_Tree('training.csv', 'testing.csv', 'entropy', 0, 1)

The two above lines produce the same result when initializing the tree. The
initialization inludes a call for the training dataset, testing dataset, and 
optional calls for impurity measure, significance level, and threshold level.

The impourity measure takes 'entropy' as a default and uses the entropy
calculation in Mitchell to calculate the entropy in the current data
subset. If the user enters 'entropy' the same result will be produced.
If the user enters any other string, this will use the gini index to 
calculate information gain. Thus althought 'gini' is the recommended
entry for this ititialization, really even an entry of 'a' would produce
the same result.

For the significance threshold, the user should enter a value between 0 and 1.
This is the area under the curve needed to calculate the p-value coming from
the chi-squared distribution when the user decided to prune. A value of 0 will
result in skipping the chi-squared calculation with the effect of no pruning
occuring. (The test statistic will always be to the left of the p-value in this
case). The default for this entry is 

For the proportion threshold, the default is 0.91125. This value should be given
between 0 and 1. This entry is used to set a requirement for a node to be a leaf
node. If one of the truth values (in this case 'N', 'IE', or 'EI') makes up a
proportion of the total node dataset greater than this threshold then the node
will be considered a leaf node. A value of 0 means that if at least 0 of the
total record are a truth value, then this is a leaf node and the bigest 
proportion (closest to 1) of the truth values will be assigned to the node. This
in turn  will result in a classification at the node when a record arrives of
that truth value. Similarly for 1, we only assigned the node a leaf node
designation if all the value are of one truth value.

A couple other example initializations are 
dt = Decision_Tree('training.csv', 'testing.csv', 'entropy', 0.95, 0.91125)
dt = Decision_Tree('training.csv', 'testing.csv', 'gini', 0, 0.92)

After initializing the decision try we move on to actually building the tree.

object.build_tree(object.data_list)

The line above builds a decision tree from the data in the object.data_list
attribute list. This will build a tree that can be used to classify test data.
There should be very little deviation in this method call.

object.classify_test()

To classify a set of data we simply called the classify_test method. This will
produce a .csv file of the decision trees classification of each record in the
testing data csv file that was pulled in earlier. This file can then be
considered for use with the accuracy found in validation studies on the decision
tree.

If you need to contact us about anything in this code you can reach us at
czapo@unm.edu (Christopher Zapotocky)
fviramontes8@unm.edu (Francisco Viramontes)