#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#*******************************************************************************
# Authors: Francisco Viramontes and Christopher Zapotocky
# Last Modified: September 16, 2018
# Program Name: dt.py
# Program Purpose: This program builds a class with all the self contained
# functions needed to make a decision tree. The program is used as a base
# for parsing a set of data 
#*******************************************************************************

import math #math.log(x[,base])
import pandas as pd #pd.read_csv('_.csv')
import numpy as np
np.set_printoptions(threshold=np.nan)
import copy
import time
import random
#from scipy.stats import chi2

'''
Decision Tree notes
Examples: Training Samples
Target_attribute: Attribute in which the value must be predicted
Attributes: List of other attributes that may be tested
ID3_class(Examples, Target_attribute, Attributes)
    Create root node
    If all examples are pos, return root label pos
    If all examples are neg, return root label neg
    If Attributes is empty, return root with label most common value of
    Target_attribute
    Otherwise:
        A = Attributes that best classify examples
        Root = A
        for v in A:
            add new branch below root coresponding to test A = v
            let Examples
'''
#In general comments will be above the line they are commenting on
class Decision_Tree:
    def __init__(self, data_location, data_location_test, gain_type = 'entropy', significance_threshold = 0.95, threshold = 0.91125):
        '''
        Description:
        Initializes the Decision_Tree class.
        
        Input:
        data_location -> string
        data_location_test -> string
        gain_type -> string
        significance_threshold -> double between 0 and 1
        
        Output:
        None

        example: learning_test = Decision_Tree('training.csv', 'testing.csv', 'entropy', 0.950)
        '''
        #These are the column names for the Dataset in our case ID, Seq, and Val
        self.column_names = []
        #A storing of the *training* data in a pandas dataframe into the class
        self.data = self.quick_get_data(data_location)
        #A storing of the *testing* data in a pandas dataframe in to the class
        self.data_test = self.quick_get_data(data_location_test)
        #node_list is used to store the nodes of the tree throughout the program
        self.node_list = []
        #i_set is used to just to set the index in the pandas dataframe
        self.i_set = None
        #The data we use is in the form of a list we pass from the dataframe to
        #a list here for the training data
        self.data_list = self.data[[self.column_names[1],self.column_names[2]]].values.tolist()
        #The data we use is in the form of a list we pass from the dataframe to
        #a list here for the *test* data
        self.data_list_test = self.data_test[[self.column_names[0],self.column_names[1]]].values.tolist()
        #Option to switch between entropy or gini indexing as the information source
        self.gain_type = gain_type
        #This is the alpha value used in the chi-square test statistic call
        self.alpha_threshold = 1 - significance_threshold
        self.sig_threshold = significance_threshold
        self.prop_thresh = threshold

        #This function clears all uncertain DNA values in the *training* set
        #by assigning them random values from the set of possible value for
        #the letter D, N, S, and R.
        self.data = self.clear_excess_vars(self.data)
        #This function clears all uncertain DNA values in the *testing* set
        #by assigning them random values from the set of possible value for
        #the letter D, N, S, and R.        
        self.data_test = self.clear_excess_vars(self.data_test)

    def get_data(self, data_location):
        '''
        Description:
        This function simply pulls the data from a training csv given the path
        described when initializing. We also ask the user for column names in
        the data. Then we make a dataframe from the data. If they give one of
        the names to be 'ID' or 'id' then we remake that to the be the ID column
        in the dataframe (instead of the default).
        
        Input:
        data_location -> string
        
        Output:
        csv_train -> pandas dataframe
        '''
        #flag will terminate when empty string is fed into the input
        flag_set = 0
        list_of_column_headers = []

        #The while loop executes till an empty string is entered
        while flag_set != 1:
            columns_for_data = input('Please give a list of column headers ' \
                                     'you would like to use (leave empty to ' \
                                     'exit): ')
            if columns_for_data == "":
                flag_set = 1
            else:
                if columns_for_data == "ID" or columns_for_data == "id":
                    i_set = columns_for_data
                list_of_column_headers.append(columns_for_data)

        #This code shields an overwrite of column names when we bring the
        #testing data in. That is only overwrite the column names list if
        #it does not exist or if the new set is longer. The testing set
        #will be a subset of the training set headers (minus Val).
        if len(self.column_names) == 0 \
               or \
            len(list_of_column_headers) > len(self.column_names):
            self.column_names = list_of_column_headers
        
        csv_train = pd.read_csv(data_location, names = list_of_column_headers)

        #if i_set:
        #    csv_train.set_index(i_set,inplace=True)
        #    print("i_set: "+str(i_set))

        #I hard coded in these values for this dataset
        self.values =  ['IE', 'EI', 'N']
        self.a_values = ['A','C','G','T']
        
        return csv_train

    def quick_get_data(self, data_location):
        '''
        Description:
        quick_get_data is used to make a quick run through the intro to
        run a test. Frankie prefers this method. Saves a few seconds. Results
        in the same run as get_data above.
        Input:
        data_location -> string
        
        Output:
        csv_train -> pandas dataframe        
        '''
        self.column_names = ['ID', 'DNA', 'EI/IE/N']
        csv_data = pd.read_csv(data_location, names = self.column_names)
        self.values =  ['IE', 'EI', 'N']
        #clear_excess_vars() clears all the D,N,S and R wildcards replacing
        #them with random A, C, G, and Ts.
        self.a_values = ['A','C','G','T']
        #self.a_values = ['A','C','G','T','D','N','S','R']
        return csv_data

    def build_tree(self,p_node_data,p_node_number = None,n_node_number = 0):
        '''
        Description:
        To find what attribute to split on we find the entropy of each value in
        the attribute and then the one with the highest information gain
        (calculated from the entropy) gives us our choice of attribute. The tree
        creates nodes recuresively running down each branch till a purity or
        pruning condition is reached.

        Input:
        p_node_data -> list
        p_node_number -> nonnegative integer
        n_node_number -> nonnegative integer
        
        Output:
        new_node.node_number -> nonnegative integer
        '''
        #after steping into the function we create a node and pass the
        #current node number, previous node number, truth values, and data.
        new_node = Node(p_node_data, self.values, self.prop_thresh, p_node_number, n_node_number)
        #Once a node is created we append that node object to a list
        self.node_list.append(new_node)

        #This print statement helps us keep track of where we are in the tree
        #creation process.
        print("Current Node " + str(new_node.node_number))

        #If this is not the first node we want to pass the features used down
        #the tree to the next node, from the previous node. We do that here.
        if new_node.node_number > 0:
            new_node.feat_used = copy.deepcopy(self.node_list[new_node.parent].feat_used)

        #This if statement checks after the creation of the node whether a
        #purity condition has been reached.
        purity_flag = new_node.check_for_purity()

        #If the purity flag is set it means we have a terminating or leaf node.
        #Thus there is no reason to add children to it. This ends the recursion.
        if purity_flag == 1:
            pass

        #If the purity/termination requirements are not met then we add children
        #to the tree.
        else:
            gain_list = []
            #For the all the different features in the data set we want to find
            #the feature that produces the highest gain in information.
            for a in range(0,len(p_node_data[0][0])):
                single_a_list = []
                #Lets run through  the attributes one by one. We build a list of the
                #attribute across all records. That is location 0 is an attribute,
                #so we have a list of all location 0 values in one list...etc...
                for b in range(0,len(p_node_data[:])):
                    #This is the list of all the data we want to consider for
                    #our gain.
                    single_a_list.append([p_node_data[b][0][a],p_node_data[b][1]])
                temp_gain = 0
                #This returns the gain for a given feature from the dataset.
                temp_gain = self.gain(single_a_list)
                #we append the gain for each feature to a list.
                gain_list.append(temp_gain)

            #We do not want to reuse gain values so here we set gains of
            #of previously used nodes in the branch to some large negative
            #number. Here that number is 100.
            for item in new_node.feat_used:
                gain_list[item] = -100

            #We pick out the maximum gain(s) in the gain list.
            m = max(gain_list)

            #We find feature number (sequence location) of the gain value. 
            max_list_loc = [i for i, j in enumerate(gain_list) if j == m]

            #This if else statement serves the purpose of dealing with the
            #rare, but possible situation we have more than one maximum.
            #If this occures we choose a random feature from the set of
            #identical maximum feature values.
            if len(max_list_loc) > 1:
                a_index = random.randint(0,len(max_list_loc)-1)
                new_node.feat_used.append(max_list_loc[a_index])
            elif len(max_list_loc) == 0:
                print("Something went wrong while building the tree. No max \
                            entropy was found.")
            else:
                new_node.feat_used.append(max_list_loc[0])

            #This for loop creates the children node branches.
            for d in self.a_values:
                full_list = []
                for item in new_node.node_data:
                    #Check whether any elements will be passsed to the given
                    #feature value. If not then we do will not make that node.
                    if item[0][new_node.feat_used[-1]] == d:
                        full_list.append(item)
                #This is the recursion step. Here we call again the new node
                #function if that node is needed (i.e. full list is not empty)
                if len(full_list) > 0:
                    ret_child_number = self.build_tree(full_list,new_node.node_number, len(self.node_list))
                    new_node.children.append([ret_child_number,d])
                #When we are done with the making the children nodes we want to
                #find out if we should keep them. We test for statistical
                #significance using an ANOVA like process to compare the
                #proportions in each feature value.
                if d == self.a_values[-1]:
                    #Test the chilren nodes for significance as described in class
                    children_sig = self.test_sig(new_node.node_number)
                    #If the childrens proportions are not statistically sifnicantly
                    #different from the parents we prune the nodes.
                    if children_sig == 0:
                        #If there are any children to prune
                        if self.node_list[new_node.node_number].children:
                            #Then run through the children and prune them
                            for i in range(len(self.node_list[new_node.node_number].children)-1,-1,-1):
                                #Delete the child node from the node list
                                self.node_list = self.node_list[:-1]
                                #Delete the child node reference from the current node
                                self.node_list[new_node.node_number].children \
                                = self.node_list[new_node.node_number].children[:-1]
                            #Make the current node a leaf node.
                            self.node_list[new_node.node_number].purity_flag = 1                
        #We move back up the recursive tree branch when we return the current
        #node number having made a new node.
        return new_node.node_number

    def gain(self, list_data):
        '''
        Description:
        The gain function finds the gain of a set of data for a given feature.
        We can use either the entropy or gini index for doing so.

        Input:
        list_data -> list
        
        Output:
        summed_gain -> double
        '''
        #We start with the overall information value of the data given the truth
        #values.
        summed_gain = float(self.info(list_data)[0])
        #Now we will cycle through the attribute data and find the information
        #value given the attributes of the features.
        for c in range(0, len(self.a_values)):
            #I store the information value temporarily
            temp_ent_size = self.info(list_data,self.a_values[c])
            #We iteratively subract the proporational truth value given the
            #proportion of the gain to the data.
            summed_gain = summed_gain - temp_ent_size[0] *\
                        float(abs(temp_ent_size[1])) / float(temp_ent_size[2])
        return summed_gain


    def info(self, list_data, a_value = None):
        '''
        Description:
        This function takes either Gini or Entropy internally depending on what the
        user defined as their choice of information gain.

        Input:
        list_data -> list
        a_value -> char
        
        Output:
        result, value_total, entropy_total -> tuple
        '''
        if type(list_data) != type([]):
            print("This is not a list\n")
            return None
        else:
            attribute_data = copy.deepcopy(list_data)
            result = 0
            #This is the total number of records we are dealing with
            entropy_total = len(attribute_data)
            value_total = 0
            for value in self.values:
                #If we are calculating the information of an attribute
                if a_value:
                    #The value list is just a local storage of the records
                    #used for this calculation.
                    value_list = []
                    #Count is our denominator in our proportion below.
                    count = 0
                    #We scroll through all the records
                    for item in attribute_data:
                        #If we find one that is the same as the given attribute
                        #value of the feature
                        if item[0] == a_value:
                            #Then we increment the count.
                            count = count + 1
                            #If the truth value is what the one we are looking
                            #for currently.
                            if item[1] == value:
                                #Then we add it to our value list.
                                value_list.append(item[1])                        
                    #We get the length of the value list to get the proportion
                    #for the information calculation.
                    entropy_iter = len(value_list)
                    #If it is entropy we use the entropy formula
                    if self.gain_type == 'entropy':
                        if entropy_iter > 0:
                            prop_i = float(entropy_iter)/float(count)
                            result = result - (prop_i) * math.log(prop_i, 2)
                    #If it is the gini index we use the gini index formula
                    else:
                        prop_i = float(entropy_iter)/float(entropy_total)
                        result = result - math.pow(prop_i,2)
                    value_total = value_total + entropy_iter
                #else we have the complete information to find for the total
                #data. Here we do not consider the attributes of a feature.
                else:
                    #The value list here just stores the records for a given
                    #truth value agreeing with our if statement.
                    value_list = []
                    #Scroll through the whole list of records.
                    for item in attribute_data:
                        #If the truth statement is equal to our current desire
                        if item[1] == value:
                            #Then we store it in the value list.
                            value_list.append(item[1])
                    entropy_iter = len(value_list)
                    #If it is entropy we use the entropy formula
                    if self.gain_type == 'entropy':
                        if entropy_iter > 0:
                            prop_i = float(entropy_iter)/float(entropy_total)
                            result = result - (prop_i) * math.log(prop_i, 2)
                    #If it is the gini index we use the gini index formula
                    else:
                        prop_i = float(entropy_iter)/float(entropy_total)
                        result = result - math.pow(prop_i,2)
                    #Here we keep track of all the value totals over the truth
                    #values.
                    value_total = value_total + entropy_iter                    
            return result, value_total, entropy_total
    
    def test_sig(self,parent_node_number):
        '''
        Description:
        The test_sig function calculates the significance of the proportions in
        the children nodes of the current node. It produces a 1 if the
        proportions are significant and a 0 ir they are not.
        
        Input:
        parent_node_number -> nonnegative integer
        
        Output:
        significance -> boolean
        '''
        chi_stat = 0
        #Get the degrees of freedom from the number of children
        deg_f_v = len(self.node_list[parent_node_number].children) - 1
        #Get the degrees of freedom from the number of truth values.
        deg_f_c = len(self.values) - 1
        #We iterate through the children
        for v in self.node_list[parent_node_number].children:
            #We iterate through the truth values
            for t in self.values:
                #parent proportion for a given truth
                parent_v_count = 0
                for record_p in self.node_list[parent_node_number].node_data:
                    if record_p[1] == t:
                        parent_v_count = parent_v_count + 1
                parent_v_prop = parent_v_count/len(self.node_list[parent_node_number].node_data)

                #Child number for the given truth
                child_v_count = 0
                for record_c in self.node_list[v[0]].node_data:
                    if record_c[1] == t:
                        child_v_count = child_v_count + 1
                #Calculate the predicted number for a truth and child combo
                #(in the bin)
                vti_prime = float(child_v_count * parent_v_prop)

                #vti is just the actual number in that child/truth bin
                vti = child_v_count
                #Calculate the test statistic assuming we do not have a denominator
                if vti_prime > 0:
                    chi_stat = chi_stat + (math.pow(vti - vti_prime, 2) / vti_prime)
        #Here we calculate the total degrees of freedom to use in finding the
        #p value
        total_deg_f = deg_f_v * deg_f_c

        #We assume the plit was significant.
        significance = 1

        pvalue = self.chi2_test(self.sig_threshold, total_deg_f)
        #If it was not, we change the value to 0.
        if pvalue != -1:
            if chi_stat < pvalue:
                significance = 0      
        return significance
    #Just in case we cannot use scipy to call the chi2 function.
    def chi2_test(self,cl,deg):
        '''
        Description:
        The chi2_test is a table of values for selecting chi-squared p-value
        for comparing the test static to determine whether we should prune the
        child values.
        
        Input:
        cl -> double from 0 to 1
        deg -> positive integer (from 0 to 60)
        
        Output:
        output -> double 0 to inf
        
        '''
        #Columns are for 0.99, 0.95, 0.90, and 0.01 which is the probability of
        #a value larger than x^2.
        deg = deg - 1
        
        table = [[1, 6.6348966010212127, 3.8414588206941236, 2.7055434540954169, 0.00015708785790970195], \
                 [2, 9.2103403719761801, 5.9914645471079799, 4.6051701859880918, 0.020100671707002898], \
                 [3, 11.344866730144371, 7.8147279032511765, 6.2513886311703226, 0.11483180189911742], \
                 [4, 13.276704135987622, 9.487729036781154, 7.7794403397348582, 0.29710948050653174], \
                 [5, 15.086272469388987, 11.070497693516351, 9.2363568997811214, 0.55429807672827647], \
                 [6, 16.811893829770927, 12.591587243743977, 10.64464067566842, 0.87209033015658677], \
                 [7, 18.475306906582361, 14.067140449340167, 12.017036623780527, 1.2390423055679316], \
                 [8, 20.090235029663233, 15.507313055865453, 13.36156613651173, 1.6464973726907703], \
                 [9, 21.665994333461921, 16.918977604620448, 14.683656573259837, 2.0879007358707269], \
                 [10, 23.209251158954356, 18.307038053275143, 15.987179172105263, 2.5582121601872085], \
                 [11, 24.72497031131828, 19.675137572682491, 17.275008517500073, 3.0534841066406782], \
                 [12, 26.216967305535849, 21.026069817483066, 18.549347786703244, 3.570568970604393], \
                 [13, 27.688249610457049, 22.362032494826941, 19.811929307127564, 4.1069154715044078], \
                 [14, 29.141237740672796, 23.68479130484058, 21.064144212997064, 4.6604250626577706], \
                 [15, 30.577914166892494, 24.99579013972863, 22.3071295815787, 5.2293488840989619], \
                 [16, 31.999926908815176, 26.296227604864235, 23.541828923096112, 5.8122124701349716], \
                 [17, 33.40866360500462, 27.587111638275324, 24.769035343901454, 6.4077597777389332], \
                 [18, 34.805305734705072, 28.869299430392626, 25.989423082637209, 7.0149109011725779], \
                 [19, 36.190869129270048, 30.143527205646159, 27.20357102935683, 7.6327296475714768], \
                 [20, 37.56623478662506, 31.410432844230929, 28.411980584305635, 8.2603983325463997], \
                 [21, 38.932172683516072, 32.670573340917294, 29.615089436182739, 8.8971979420772147], \
                 [22, 40.289360437593864, 33.9244384714438, 30.813282343953045, 9.5424923387850757], \
                 [23, 41.638398118858483, 35.17246162690806, 32.00689968170429, 10.195715555745826], \
                 [24, 42.979820139351631, 36.415028501807299, 33.196244288628193, 10.856361475532283], \
                 [25, 44.314104896219149, 37.652484133482766, 34.381587017552931, 11.523975372249339], \
                 [26, 45.641682666283153, 38.885138659830055, 35.56317127192348, 12.198146923505591], \
                 [27, 46.962942124751436, 40.113272069413611, 36.741216747797665, 12.878504393144546], \
                 [28, 48.278235770315483, 41.337138151427411, 37.915922544697111, 13.564709754618821], \
                 [29, 49.587884472898793, 42.556967804292668, 39.087469770693943, 14.256454576274679], \
                 [30, 50.892181311517071, 43.772971825742182, 40.256023738711775, 14.95345652845544], \
                 [31, 52.191394833191922, 44.985343280365129, 41.42173582978522, 15.655456401681393], \
                 [32, 53.485771836235344, 46.194259520278457, 42.584745082980817, 16.362215547665798], \
                 [33, 54.775539760110341, 47.399883919080921, 43.745179559434206, 17.073513672329398], \
                 [34, 56.060908747789064, 48.602367367294178, 44.903157518519926, 17.789146923546884], \
                 [35, 57.342073433859184, 49.801849568201824, 46.058788436836636, 18.50892622702494], \
                 [36, 58.619214501687026, 50.99846016571064, 47.212173894937322, 19.232675832154086], \
                 [37, 59.892500045086919, 52.192319730102874, 48.363408352194355, 19.960232036407152], \
                 [38, 61.162086763689707, 53.383540622969278, 49.512579826575575, 20.691442062257163], \
                 [39, 62.42812101618491, 54.572227758941736, 50.659770493213728, 21.426163064945897], \
                 [40, 63.690739751564443, 55.758479278887037, 51.805057213317497, 22.164261252975159], \
                 [41, 64.950071335211177, 56.942387146824075, 52.948512003082044, 22.905611106081128], \
                 [42, 66.206236283993221, 58.124037680867971, 54.090202450712411, 23.650094677826203], \
                 [43, 67.459347922325833, 59.303512026899838, 55.230192088408906, 24.39760097189745], \
                 [44, 68.709512969345383, 60.480886582336431, 56.368540725118777, 25.148025382824503], \
                 [45, 69.956832065838171, 61.656233376279538, 57.505304744995946, 25.901269193178042], \
                 [46, 71.201400248311515, 62.829620411408186, 58.640537375791695, 26.657239120440895], \
                 [47, 72.443307376548248, 64.001111972218013, 59.774288930795947, 27.415846907690145], \
                 [48, 73.682638520105698, 65.170768903569808, 60.906607027448331, 28.177008953028878], \
                 [49, 74.919474308478158, 66.338648862968768, 62.037536785309584, 28.94064597338151], \
                 [50, 76.153891249012673, 67.5048065495412, 63.167121005726329, 29.706682698841281], \
                 [51, 77.385962016137313, 68.669293912285838, 64.295400335215902, 30.475047594247478], \
                 [52, 78.615755715002507, 69.832160339848173, 65.422413414339772, 31.245672605088203], \
                 [53, 79.843338122251481, 70.993452833782186, 66.548197013609212, 32.018492925182898], \
                 [54, 81.06877190629713, 72.153216167023089, 67.672786157777495, 32.793446783909019], \
                 [55, 82.292116829199671, 73.311493029083195, 68.796214239709229, 33.570475251000268], \
                 [56, 83.513429931989407, 74.46832415930939, 69.918513124876483, 34.349522057178135], \
                 [57, 84.732765705063926, 75.623748469376167, 71.039713247404364, 35.130533429075484], \
                 [58, 85.950176245103407, 76.777803156061395, 72.159843698492011, 35.913457937085212], \
                 [59, 87.165711399787568, 77.930523805230379, 73.278932307930887, 36.698246354920627], \
                 [60, 88.379418901449412, 79.081944487848745, 74.397005719368636, 37.484851529803777]]
        k = None
        
        if cl == 0.99:
            k = 1
        elif cl == 0.95:
            k = 2
        elif cl == 0.90:
            k = 3
        elif cl == 0.01:
            k = 4
        elif cl == 0:
            pass
        else:
            print("Please pick a confidence level of 0.99, 0.95, 0.90, or 0.01.")
        if k:
            output = table[deg][k]
        else:
            output = -1
            
        return output
    
    def clear_excess_vars(self, data_struct):
        '''
        Description:
        The clear_excess_vars replaces each one of the unknown nucleotides
        values in the sequence with a random assignement of equal probability
        between the posisble nucleotide values.

        See https://archive.ics.uci.edu/ml/datasets/Molecular+Biology+(Splice-junction+Gene+Sequences)

        character: meaning
        D: A or G or T
        N: A or G or C or T
        S: C or G
        R: A or G
        
        Input:
        data_struct -> pandas dataframe
        
        Output:
        fixed_data -> pandas dataframe
        '''
        #The if statement turns the string to a list, and replaces the letter
        #with a random possible letter. We then turn the list back into a string
        #and replace the sequence in the record we took it from.
        for a in range(len(data_struct.index)):
            temp_string = data_struct.iat[a,1]
            temp_list = list(temp_string)
            for b in range(len(temp_list)):
                if temp_list[b] == 'D':
                    temp_list[b] = random.choice(['A','G','T'])
                elif temp_list[b] == 'N':
                    temp_list[b] = random.choice(['A','C','G','T'])
                elif temp_list[b] == 'S':
                    temp_list[b] = random.choice(['C','G'])
                elif temp_list[b] == 'R':
                    temp_list[b] = random.choice(['A','G'])
                else:
                    pass
 
            fixed_data = data_struct.iat[a,1] = "".join(temp_list)
        return fixed_data

    def classify_test(self):
        '''
        Description:
        classify_test runs through the prebuilt tree for every record and gives
        it a classification truth value.

        Input:
        None
        
        Output:
        None
        '''
        current_node = 0
        classify_list = []
        #If there are is at least one node.
        if len(self.node_list) > 0:
            #Then run through the data and assign a classification value, to the
            #list for for each record depending on the leaf node where it ends. 
            for record in self.data_list_test:
                classify_list.append([record[0],self.next_node(current_node,record)])

            #This is where we send the list to a file for distribution to Kaggle
            classify_file = open("classify.csv",'w')
            classify_file.write("id,class\n")
            for element in classify_list:
                classify_file.write(str(element[0]) + "," + str(element[1]) + "\n")
            classify_file.close()
        else:
            print("You have not built your tree yet. Please use the build_tree \
                    function to build it.")

    def next_node(self,current_node,record):
        '''
        Description:
        The next_node function is a recursilvely called function that takes the
        assigned record all the way down a given branch of the the tree. It is
        the primary workhorse function of the classify_test function.

        Input:
        current_node -> nonnegative integer
        record -> list
        
        Output:
        result -> string
        '''
        #If we are at a leaf node then return the result of the truth value
        #at that leaf node. This will be passed recursively back up the
        #branch to classify_list for storage.
        if self.node_list[current_node].purity_flag == 1:
            result = self.node_list[current_node].classify
        #Else if we are not at a leaf node.
        else:
            #Then find what feature was used in this node
            current_feature = self.node_list[current_node].feat_used[-1]
            #And which child we need to go to next based on the attribute
            #value at that feature.
            branch_choice = record[1][current_feature]                
            test_node = current_node
            current_child_number = 0
            #We iterate through the list of children nodes with an index.
            while (test_node == current_node) and (current_child_number < len(self.node_list[current_node].children)):
                #If our branch choice above equals the child nodes number
                if self.node_list[current_node].children[current_child_number][1] == branch_choice:
                    #Set the current_node equal to the child node
                    current_node = self.node_list[current_node].children[current_child_number][0]
                    #Thhen recursively call next_node again
                    result = self.next_node(current_node,record)
                #Else we increment the children list index to check.
                else:
                    current_child_number = current_child_number + 1
            #If we could not find a child node to go to next use the truth
            #value of the current node for the record.
            if current_child_number >= len(self.node_list[current_node].children):
                result = self.node_list[current_node].classify
            #We pass the result recursively back to classify list.
        return result

                              
class Node(Decision_Tree):
    def __init__(self, data_set, values, threshold, previous_node = None, number = 0):
        '''
        Description:
        The Node function is the primary function used to build the tree.
        Essentially it builds a new node given the selected feature in the
        previous node. This is done branch by branch recursively.

        Input:
        data_set -> list
        values -> list of strings
        previous_node -> nonnegative integer
        number -> nonnegative integer
        
        Output:
        None       
        '''
        #Below are the parameters we need to build nodes in the tree
        #We need the parent node to know where we came from in the branch
        self.parent = previous_node
        #We need a list of children nodes to check where we should go
        self.children = []
        #We need the data_set handed off in each recursive call to the child node
        self.node_data = data_set
        #We need a node number that is stored in a list of objects so we can
        #call the object from the node_list.
        self.node_number = number
        #The features used list keeps track of the features used in this branch
        #up to this node.
        self.feat_used = []
        #The classify variable stores the classification value we assign for
        #this node assuming our classification search stops at this node.
        self.classify = None
        #The prop variable is the proportion of the a given truth value with the
        #largest proportion at this node.
        self.prop = None
        #The purity flag says whether one of the purity requirements or stopping
        #criteria were met for this node.
        self.purity_flag = 0
        #Additional storage of values needed for the node class to inherit the
        #truth values.
        self.values = values
        self.prop_thresh = threshold
        
    def check_for_purity(self):
        '''
        Description:
        The check_for_purity function is used to check to see if the purity
        requirement or a stopping criteria for the node is met. It is run
        after the node has been created.

        Input:
        None
        
        Output:
        self.purity_flag -> boolean
        '''
        #Get the total length of the data stored in the object.
        total_length = len(self.node_data)
        #If there is only one record left set the truth value for
        #this node to that value and mark it as a leaf node.
        if total_length == 1:
            self.classify = self.node_data[0][1]
            self.prop = 1
            self.purity_flag = 1

        #If we have some other case
        best_proportion = 0
        best_type = None
        #Check each possible truth value for each record
        for v in self.values:
            count = 0
            for item in self.node_data:
                #If we have a truth value for a record count it.
                if item[1] == v:
                    count = count + 1
            #With the count if all the records in the node are of one truth
            #value as those records truth value and make the node a leaf node.
            if count == total_length:
                self.classify = self.node_data[0][1]
                self.prop = 1
                self.purity_flag = 1
            #If we have used up all the possible features then we can no longer
            #make new nodes. So we mark this as a leaf node and set the truth
            #value to the proportion highest amongst the truth value.
            elif len(self.feat_used) == len(self.node_data[0][0]):
                proportion = float(count)/float(total_length)
                if proportion > best_proportion:
                    best_proportion = proportion
                    self.classify = v
                    self.prop = best_proportion
                self.purity_flag = 1
            #This is a pruning parameter. If any of our truth value proportions
            #are above a threshold for the node we set the node as a leaf node
            #and the classification value as the truth value.
            elif float(count)/float(total_length) > self.prop_thresh:
                proportion = float(count)/float(total_length)
                if proportion > best_proportion:
                    best_proportion = proportion
                    self.classify = v
                    self.prop = best_proportion
                self.purity_flag = 1
            #We need to calculate the proportion of best truth at each node
            #because we may not have a full set of children nodes. Which means
            #our classification may need to occur here. This covers that
            #situation.
            else:
                proportion = float(count)/float(total_length)
                if proportion > best_proportion:
                    best_proportion = proportion
                    self.classify = v
                    self.prop = best_proportion
        return self.purity_flag

#This runs the classes assuming a training.csv and testing.csv are defined.
if __name__ == '__main__':
    learning_test = Decision_Tree('training.csv', 'testing.csv', 'entropy', 0, 1)
    #print(learning_test.data)
    learning_test.build_tree(learning_test.data_list)
    learning_test.classify_test()
