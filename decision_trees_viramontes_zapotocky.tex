\documentclass[10pt]{extarticle}

\usepackage{geometry}
\geometry{lmargin=2cm}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}

\usepackage{float}

\usepackage{titling}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\setlength{\droptitle}{-10em}

\usepackage{multicol}
\setlength{\columnsep}{0.5cm}

\usepackage{titling}
\setlength{\droptitle}{-3cm}

\usepackage{graphicx}
\graphicspath{ {/home/franku/CS529_Decision_Trees/} }
\usepackage{fancyhdr}

%bibtex
\usepackage[backend=bibtex]{biblatex}
\bibliography{dt}

%details for \tab command
%\newcommand\tab[1][0.5cm]{\hspace*{#1}}

% headers
\fancyhead[L]{Francisco Viramontes}
\fancyhead[C]{CS 529 Project 1: Decision Trees}
\fancyhead[R]{Christopher Zapotocky}

% need to specify the pagestyle as fancy
\pagestyle{fancy}

\title{{\Huge \textbf{CS 529\\Project 1: Decision Trees}}}

\author{\huge Francisco Viramontes, Christopher Zapotocky\\Dept. of Electrical and Computer Engineering \\ University of New Mexico\\ \{fviramontes8, czapo\}@unm.edu}

\begin{document}
\maketitle
\begin{multicols}{2}

\section*{Introduction}
\tab A decision tree is very similar to a binary tree, there is a root node and it can have multiple layers of child and parent nodes below the root node, and the end nodes are leaves. Decision trees are one of the most used applications in Machine Learning for inductive inference\cite{Mitchell:1997}. This is done by making an approximation of a discrete function. It is mostly used for categorical classification, separating properties and attributes into distinct categories.\\
\tab The advantage of using decision tree learning is that data is represented in a pair of attributes and values (e.g. $[a_1, \{v_1, v_2, ..., v_n\}]$). Where \emph{a} is attributes and \emph{v} is values, and \emph{n} is the number of distinct categorical values. Another advantage is that decision tree learning can produce outputs that are discrete which may make visualization and understanding easier. Additionally, the training set, including attributes and values, can have errors and ambiguity without having a major effect on the accuracy of the model, meaning that the model is robust.

\section*{Background}
\tab The creation of a decision tree starts by testing each attribute and creates a node that describes the set of possible values from the best attribute. This process is replicated until it reaches a node that contains values from one category. This node then becomes a leaf node and determines that the attribute of the leaf node represents a label that is either positive or negative. The creation of the decision tree is finished when all leaf nodes have been realized.\\
\tab The decision tree algorithm used here is ID3\cite{Quinlan:1983}, which begins with a top-down approach. First it uses a metric for measuring impurity and chooses the attribute that will provide the most information. \\
\tab Two ways to determine metrics for measuring impurity are presented:
\subsection*{Entropy}
\tab One way to measure the impurity is by using a method used often in information theory: entropy. It is described as:
\[H(S) = - \sum^c_{j=1}P_jlog_2(P_j)\]
Where $P_j$ is the $j^{th}$ proportion of the value of the current attribute, \emph{S}, being tested, \emph{c} is the set of all values that is being tested. Entropy in the context of the ID3 algorithm is used to determine the uncertainty of the attribute being tested.
\subsection*{Gini-Index}
Gini-Index can be classified as such:
\[G(S) = 1-\sum^c_{j=1}P^2_j\]
\tab Which is similar to Entropy, $P_j$ is the $j^{th}$ proportion of the value of the current attribute, \emph{S}, being tested, \emph{c} is the set of all values that is being tested. However, Gini-Index is computationally less expensive since the equation does not to calculate the result of a logarithm.
\subsection*{Information Gain}
\tab In order to determine which attribute to classify next, it is necessary to calculate how much information are we going to get from testing a specific attribute. To choose the best attribute, the ID3 algorithm selects the attribute with the highest information gain (IG). \[IG(S, A) = Impurity(S) - \sum^c_j \frac{ |S_j|}{S}Impurity(S_j)\] 
\tab Where \emph{S} is the set of all attributes, \emph{A} is an attribute from set \emph{S}, \emph{c} is the set of all values in attribute \emph{A}, and \emph{j} is the $j^{th}$ value in set \emph{c}. This is used at each step to create a node. This is done until a leaf node is created: where the values of the attribute belong to one categorical value (e.g. in Boolean classification, all values are either positive or negative).\\
\tab The span of the evaluation space of the ID3 algorithm is a finite set of discrete attributes, and in its purest form, it does not perform any backtracking.The building of the tree is able to favor on building short trees, rather than large complex ones. This can cause a problem because the decision tree might focus on locally optimal solution when a globally optimal solution may lead to better accuracy in the classification of new data. This may also lead to overfitting, where the model performs extraordinarily well with the training data, but performs poorly when presented with new data.\\
\tab One way to get around this is to modify the ID3 algorithm and allow for an application of a statistical test to determine whether expanding a particular node is likely to produce an improvement, proposed in \cite{Quinlan:1986}, the chi-squared test estimates the likelihood of a node improving the performance over the entire dataset, or only the current data point. The chi-squared test is described as:
\[\chi^2=\sum_i^k \frac{(x_i-\mathbb{E}\{x_i\})^2}{\mathbb{E}\{x_i\}}\]
\tab Where $x_i$ is the observation made by the decision tree, $\mathbb{E}{\{x_i}\}$ is the expectation of the observation made by the decision tree, and \emph{k} is the total number of observations made from the attribute (e.g. k = 2 for binary classification).\\
\tab The algorithm proposed in the paper is a modification of the ID3 algorithm, as the original algorithm does not use the \emph{Gini-Index}, nor does it use the \emph{Chi-squared test} to determine node performance. The initialization of the algorithm can be described as such:\\
\hrule \vspace{0.1cm}
\textbf{Algorithm 0} Decision\_Tree\_Initialize (Training, Testing, Gain, Significance\_Threshold) \vspace{0.1cm}
\hrule \vspace{0.1cm}
\vspace{0.25cm}
\noindent Tr\_data $\leftarrow$ Training\\
Tst\_data $\leftarrow$ Testing\\
data\_list $\leftarrow$ [Tr\_data.attributes, Tr\_data.values]\\
data\_list\_tst $\leftarrow$ [Tst\_data.attributes]\\
gain $\leftarrow$ Gain\\
threshold $\leftarrow$ Significance\_Threshold\\\\
\textbf{for} element \textbf{in} data\_list \textbf{do}\\
\tab \textbf{if} element $\in$ \{D, N, S, R \} \textbf{do}\\
\tab \tab element $\leftarrow$ clear\_ambiguity(element)\\
\tab \textbf{end if}\\
\textbf{end for}\\
\hrule \vspace{0.1cm}

\section*{Experimental Setup}
\tab The dataset used to test the algorithm is a gene sequence used to determine intron/extron bounderies in DNA\cite{MLData}, where they can have intron or extron boundaries, or neither. The set of genetic nucleobases for DNA are A, C, G, and T. However, in this dataset there is some ambiguity. Certain letters represent that there is a chance that the nucleobase may not be consistent. It is represented as:
\[D \in \{A, G,T\} \]
\[N \in \{A, G, C, T\} \]
\[S \in \{C, G\} \]
\[R \in \{A, G\} \]
\tab \textbf{Algorithm 0} describes how this ambiguity is solved. The program assumes that the input is comma separated values file (csv) for both training and testing values. The program provides an output of a csv file that contains 1190 rows of the decision tree's prediction. The dataset itself is then separated into a 2000 x 2 matrix, with a DNA sequence that is 60 characters long as the first column, and the second column describes whether the DNA sequence has intron or extron boundaries, or neither. An example can be shown as:
\[\left[{\begin{array}{cc}
AAAAA \hdots TCTGA & N\\
AAAAA \hdots ACCAA & N\\
AAAAA \hdots ATAAA & N\\
AAAAA \hdots ATAAA & IE\\
\vdots & \vdots\\
GCTGA \hdots CACAG & N\\
GCTGA \hdots AGAGT & EI\\
GCTGA \hdots TCGCT & IE\\
\end{array}}\right]\]
After the ambiguity is solved, \textbf{Algorithm 1} then begins to build tree.\\
\hrule \vspace{0.1cm}
\textbf{Algorithm 1} Decision\_Tree\_Build (parent\_data, parent\_node\_number, child\_node\_number) \vspace{0.1cm}
\hrule \vspace{0.1cm}
\vspace{0.25cm}
\noindent create\_node(parent\_data, values, parent\_node\_number, child\_node\_number)\\
\textbf{if} node\_list \textit{empty} \textbf{do}\\
\tab create\_root\_node()\\
\tab node\_list[0] $\leftarrow$ node\\
\textbf{else}\\
\tab node\_list[node\_length + 1] $\leftarrow$ new\_node\\
\textbf{end if}\\
node.data $\leftarrow$ parent\_data\\
purity\_flag $\leftarrow$ check\_purity(node.data)\\
\textbf{if} purity\_flag == 1 \textbf{do}\\
\tab end \textbf{Alogorithm 1}\\
\tab /*This is a leaf node*/\\
\textbf{else}\\
\tab max\_gain $\leftarrow$ find\_max\_gain(node.data)\\
\textbf{end if}\\
\textbf{for} branch \textbf{do}\\
\tab \textbf{if} node does not have children \textbf{do}\\
\tab \tab Decision\_Tree\_Build (parent\_data, parent\_node\_number, child\_node\_number)\\
\tab \textbf{else}\\
\tab \tab significance $\leftarrow$ test\_significance(node.data, parent\_node\_number)\\
\tab \tab \textbf{if} significance == 0 \textbf{do}\\
\tab \tab \tab remove\_child\_node()\\
\tab \tab \textbf{end if}\\
\tab \textbf{end if}\\
\textbf{end for}\\
\hrule \vspace{0.1cm}
Where check\_purity() finds if all values fit into one categorical group, and find\_max\_gain() can use either entropy or gini-index to determine which attribute has the most information gain. Once the tree is built, the model can be given testing data so that it can classify whether the DNA sequence testing set has intron or extron boundaries, or neither. It works by running through the classification data until it reaches a leaf node. Once the model reaches a leaf node it returns a classification value corresponding to the node. In \textbf{Algorithm 0} training and testing values are initialized, \textbf{Algorithm 1} then creates a decision tree model and \textbf{Algorithm 2} takes the model and makes a classification on the test data initialized from \textbf{Algorithm 0}.\\
\hrule \vspace{0.1cm}
\textbf{Algorithm 2} Decision\_Tree\_Classify (model, node, tst\_data) \vspace{0.1cm}
\hrule \vspace{0.1cm}
\vspace{0.25cm}
\noindent \textbf{if} number\_of\_nodes == 0 \textbf{do}\\
\tab return error('no training model')\\
\textbf{end if}\\
\textbf{if} node == leaf\_node \textbf{do}\\
\tab return node.classify\_value\\
\textbf{else}\\
\tab next\_node $\leftarrow$ find\_next\_node(node, tst\_data.attributes)\\
\tab Decision\_Tree\_Classify (model, next\_node, tst\_data)\\
\textbf{end if}\\
\hrule \vspace{0.1cm}
\section*{Results}
Creating a decision tree model from \cite{MLData}, the model has been tested with significance threshold value of 0.99, 0.95, and 0.00. The significance threshold values are supposed to test the chi-squared confidence levels. The model has a 90.336\% accuracy with 0.99 confidence, 92.016\% accuracy with 0.95 confidence, and 87.605\% accuracy with 0.00 confidence. More details on the accuracies obtained under various settings can be found in figures 1, 2, 3. However, confidence level testing does not work well with Gini-index, it maintains the same accuracy given various confidence values of 0.9, 0.99, and 0.95. Entropy and Gini-index performed similarly in having a small difference in accuracies.

\section*{Conclusion}
Decision tree learning performs well when it comes to classifying if DNA sequences with a maximum accuracy of 92\%. Performance should increase if a random forest is implemented, which is a combination of decision trees in which the input data is slightly varied for each decision tree. An ideal future experiment includes reducing the length of the DNA sequence from 60 to a smaller number like 10 or 20 (this value can be tinkered with), and building a random forest. While the random forest can improve accuracy, it is unfeasible to attain complete accuracy.
\end{multicols}
\section*{Figures}
\begin{figure}[h]
\includegraphics[scale=0.6]{99_confidence}
\caption{Decision tree prediction with 0.99 confidence for chi-squared test}
\end{figure}
\begin{figure}[h!]
\includegraphics[scale=0.6]{95_confidence}
\caption{Decision tree prediction with 0.95 confidence for chi-squared test}
\end{figure}
\begin{figure}[h!]
\includegraphics[scale=0.6]{no_confidence}
\caption{Decision tree prediction with 0.00 confidence for chi-squared test}
\end{figure}
\begin{figure}[h!]
\includegraphics[scale=0.6]{entropy_kaggle}
\caption{Decision tree prediction only using entropy for information gain, no pruning}
\end{figure}
\begin{figure}[h!]
\includegraphics[scale=0.6]{gini_full_tree}
\caption{Decision tree prediction only using gini-index for information gain, no pruning}
\end{figure}
\begin{figure}[H]
\includegraphics[scale=0.6]{gini_classify}
\caption{Decision tree prediction only using gini-index for information gain, 0.9, 0.99, 0.95 confidence for chi-squared test}
\end{figure}
\newpage
\newpage
\newpage
\printbibliography
\end{document}